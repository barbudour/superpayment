export default function ({ $axios, store }) {
    $axios.onRequest((config) => {
        console.log(store.state);
        config.headers.common['token'] = store.state.user.token
    })
}