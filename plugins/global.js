import Vue from 'vue'

import BtnBig from '@/components/landing/btn-big'
import LandingForm from '@/components/landing/landing-form'
import AccountCard from '@/components/dashboard/account-card'
import Btn from '@/components/dashboard/btn'
import HistoryCard from '@/components/dashboard/history-card'
import Modal from '@/components/dashboard/modal-wrapper'
import ModalDeposit from '@/components/dashboard/modal-deposit'
import ModalTransfer from '@/components/dashboard/modal-transfer'
import Invoice from '@/components/dashboard/invoice'
import InvoiceMessage from '@/components/dashboard/invoice-message'

Vue.use('btn-big', BtnBig)
Vue.use('landing-form', LandingForm)
Vue.use('account-card', AccountCard)
Vue.use('btn', Btn)
Vue.use('history-card', HistoryCard)
Vue.use('modal-wrapper', Modal)
Vue.use('modal-deposit', ModalDeposit)
Vue.use('modal-transfer', ModalTransfer)
Vue.use('invoice', Invoice)
Vue.use('invoice-message', InvoiceMessage)