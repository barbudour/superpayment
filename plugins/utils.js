const utils = {
    name: 'utils',
    format_price(sum, currency) {
        return (sum / 100).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,") + ` ${currency ? currency : ''}`
    }
}

export default ({ app }, inject) => {
    inject('utils', utils)
}