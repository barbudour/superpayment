FROM node:12
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

CMD ["node", "./node_modules/nuxt-start/bin/nuxt-start.js"]
