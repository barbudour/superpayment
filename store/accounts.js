export const state = () => ({
    list: [],
    active: {}
})

export const getters = {
    account: state => id => {
        return state.list.find(a => a._id === id)
    },
    list: state => {
        return state.list
    },
    get_account: state => currency => {
        return state.list.find(a => a.currency === currency)
    }
}

export const mutations = {
    SET_ACCOUNTS: (state, accounts) => {
        state.list = accounts
    },
    SET_ACTIVE: (state, active) => {
        state.active = active
    }
}

export const actions = {
    async GET_ALL_ACCOUNTS({ commit }) {
        const data = await this.$axios.$get(`/api/app/accounts`)
        commit('SET_ACCOUNTS', data.accounts)
    },
    async GET_EXCHANGE_ACCOUNTS({ commit }, id) {
        const data = await this.$axios.$get(`/api/app/account/${id}/exchange/option`)
        return data
    },
    async PAY({ commit, dispatch }, data) {
        const pay = await this.$axios.$get(`/api/app/account/${data.id}/bill?amount=${data.amount * 100}`)
        dispatch('GET_ALL_ACCOUNTS')
        return pay
    },
    async TRANSFER({ commit, dispatch }, data) {
        const transfer = await this.$axios.$post(`/api/app/account/${data.from}/transfer`, {
            amount: data.amount * 100,
            toAccountId: data.to,
            note: data.note || ''
        })
        dispatch('GET_ALL_ACCOUNTS')
        return transfer
    },
    CHANGE_ACTIVE({ commit }, data) {
        commit('SET_ACTIVE', data)
    },
    async PAY_INVOICE({ commit }, data) {
        const pay = await this.$axios.$post(`/api/app/invoice/${data}/pay`)
        return pay
    }
}