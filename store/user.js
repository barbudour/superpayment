export const state = () => ({
    auth: false,
    data: {},
    token: ''
})

export const getters = {
    email: state => {
        return state.data.email
    },
    id: state => {
        return state.token
    },
    auth: state => {
        return state.auth
    },
}

export const mutations = {
    UPDATE_USER: (state, user) => {
        state.data = user
        state.auth = true
    },
    UPDATE_TOKEN: (state, token) => {
        state.token = token
        state.auth = true
    },
    LOGOUT: (state) => {
        state.data = {}
        state.token = ''
        state.auth = false
    }
}

export const actions = {
    async LOGIN_USER({ commit }, data) {
        const user = await this.$axios.$post(`/api/app/login`, {
            username: data.email,
            password: data.password
        })
        if (user.err === null) {
            commit('UPDATE_USER', user.user)
            commit('UPDATE_TOKEN', user.token)
            this.$axios.setHeader('token', user.token)
        }
        return user
    },
    async REGISTER_EXPRESS_USER({ commit }, data) {
        const user = await this.$axios.$post(`/api/app/signup/express`, {
            email: data.email,
            name: data.first_name,
            surName: data.last_name
        })
        if (user.user) {
            commit('UPDATE_USER', user.user)
            commit('UPDATE_TOKEN', user.token)
            this.$axios.setHeader('token', user.token)
        }
        return user
    },
    LOGOUT_USER({ commit }) {
        commit('LOGOUT')
        this.$axios.setHeader('token', '')
    },
    async CHECK_USER({ commit }, data) {
        const user = await this.$axios.$get(`/api/app/user/exist?email=${data}`)
        return user.isExists
    }
}